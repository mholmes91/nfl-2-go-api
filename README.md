# Instructions

## Getting The Login Cookie
1. `cp credentials.example.json credentials.json`
2. Open credentials.json
3. Replace the placeholder text with your NFL 2 Go username and password.
4. Run `./nfl2go`

# nfl2go.com API Documentation

## API Root

* app.nfl2go.com/Player*

## Routes

### POST /GameList

* Content-Type: application/json;charset=UTF-8
* Content:
    `{"week":1,"year":2016,"type":”PRE"}`
* Type:
    * PRE - Pre-season
    * REG - Regular season
    * POST - Post season
* Week can be 1-22 (super bowl)

### POST /Watch

* Content-Type: application/json;charset=UTF-8
* Content:
    `{"code":"/GameList id field","quality":"9","type":"L"}`
* Experiment with quality
* Type:
    * N - NFL Network
    * C - Condensed
    * L - Live
    * A - Archive
* NFL Network `{"code":"1","quality":"9","type":"N"}`

# Todo
* Save cookie
* Arguments for year, week, type
