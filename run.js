var Promise = require('promise');
var request = require('request');

function getLoginCredentials(credentialsFile){
	var fs = require('fs');

	var returnPromise = new Promise(function(resolve, reject){
		fs.readFile(credentialsFile, 'utf8', function (error, string) {
			if(error){
				reject(error);
			}
			else{
				resolve(JSON.parse(string));
			}
		});
	});

	return returnPromise;
}

function getLoginCookie(username, password){
	//Get Selenium
	var webdriver = require('selenium-webdriver'),
	    By = webdriver.By,
	    until = webdriver.until;

	//Get Chrome Webdriver
	var driver = new webdriver.Builder()
	    .forBrowser('chrome')
	    .build();

	//Go to the NFL 2 Go login
	driver.get('http://nfl2go.com');

	//Enter username and password
	var usernameField = driver.findElement(By.id('Username'));
	var passwordField = driver.findElement(By.id('Password'));

	usernameField.sendKeys(username);
	passwordField.sendKeys(password);

	//Submit form
	passwordField.submit();

	//Wait for next page load
	var locator = By.xpath("//h4[contains(text(),'Your account is active now!')]");

	var waitPromise = driver.wait(until.elementLocated(locator), 5000);

	var cookieName = '.AspNet.ApplicationCookie';
	var cookieValue = null;
	var cookieDomain = '.nfl2go.com';

	//Get Cookies
	var returnPromise = waitPromise
		.then(function(){
			var cookiesPromise = driver.manage().getCookies();
			return cookiesPromise;
		})
		.then(function(cookies){
			//Get the login cookie
			for(n in cookies){
				var cookie = cookies[n];

				if(cookie.name == cookieName && cookie.domain == cookieDomain){
					cookieValue = cookie.value;
				}
			}

			//Prints
			//console.log(cookies);
			var cookieObj = {};
			cookieObj[cookieName] = cookieValue;

			return cookieObj;
		})
	;

	driver.quit();

	return returnPromise;
}

function Nfl2Go(urlBase, cookieJar){
	this.urlBase = urlBase;
	this.cookieJar = cookieJar;
}

Nfl2Go.prototype.getGameList = function(type, week, year){
	type = type ? type : 'REG';
	week = week ? week : 1;
	year = year ? year : 2016;

	var body = {
		type: type,
		week: week,
		year: year
	};

	var method = 'POST';

	var uri = this.urlBase + '/Player/GameList';

	var requestOptions = {
		method: method,
		uri: uri,
		json: body,
		jar: this.cookieJar
	}

	var returnPromise = new Promise(function(resolve, reject){
		request(requestOptions, function(error, response, body){
			if(error){
				reject(error);
			}
			else{
				resolve(body);
			}
		});
	});

	return returnPromise;
}

Nfl2Go.prototype.watch = function(code, quality, type){
	//Default to NFL Network
	if(!code && !quality && !type){
		code = 1;
		type = 'N';
	}

	quality = quality ? quality : 9;
	type = type ? type : 'A';

	var body = {
		code: code,
		type: type,
		quality: quality
	}

	var method = 'POST';

	var uri = this.urlBase + '/Player/Watch';

	var requestOptions = {
		method: method,
		uri: uri,
		json: body,
		jar: this.cookieJar
	}

	var returnPromise = new Promise(function(resolve, reject){
		request(requestOptions, function(error, response, body){
			if(error){
				reject(error);
			}
			else{
				resolve(body);
			}
		});
	});

	return returnPromise;
}

function Game(obj, nfl2go){
	for(var i in obj){
		this[i] = obj[i];
	}

	this.nfl2go = nfl2go;
}

Game.prototype.watch = function(quality, type){
    //Validation for type
	switch(type){
		case 'C':
			if(!this.IsCondensed){
				return false;
			}
			break;
		case 'L':
			if(!this.IsLive){
				return false;
			}
			break;
		case 'A':
			if(!this.IsArchive){
				return false;
			}
			break;
	}

	return this.nfl2go.watch(this.Id, quality, type);
}

getLoginCredentials('credentials.json')
	.then(function(credentials){
		return getLoginCookie(credentials.username, credentials.password);
	})
	.then(function(cookieObj){
		console.log(cookieObj);
		var cookieKeys = Object.keys(cookieObj);
		var stringifiedCookie = cookieKeys.length > 0 ? cookieKeys[0] + '=' + cookieObj[cookieKeys[0]] : '';
		var cookieJar = request.jar();
		var cookie = request.cookie(stringifiedCookie);

		var urlBase = 'http://app.nfl2go.com';
		
		cookieJar.setCookie(cookie, urlBase);

		var nfl2go = new Nfl2Go(urlBase, cookieJar);

		nfl2go.getGameList('PRE', 2, 2016).then(function(gameList){
			console.log(gameList);
			for(var i in gameList){
				var game = new Game(gameList[i], nfl2go);
				if(game.IsLive == true){
					game.watch('9', 'L').then(function(liveGame){
						return function(watchInfo){
							console.log(liveGame.Home + ' vs. ' + liveGame.Away)
							console.log('URL: ' + watchInfo);
						}
					}(game))
				}
			}
		});
	})
;
